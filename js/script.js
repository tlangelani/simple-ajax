$(document).ready(function() {

    $('#send').on('click', function(evt) {

        evt.preventDefault();

        var name = $('#name').val();

        $.ajax({
            url: 'ajax/process.php',
            type: 'POST',
            data: { name: name },
            cache: false
        }).done(function(data) {
            $('#result').html(data);
        });
        
    });
});
