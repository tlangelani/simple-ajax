<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ajax</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-1.12.1.js"></script>
    <script src="js/script.js"></script>
</head>
<body>

<div id="result"></div>

<form>
    <p>
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" required>
    </p>
    <p>
        <input type="submit" id="send" value="Send">
    </p>
</form>

</body>
</html>
